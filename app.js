  const express = require('express');
  const cors = require('cors');
  const helmet = require('helmet');
  const port = process.env.PORT || 80;
  const app = express();
  // Cruce de Origenes
  app.use(cors());
  // Seguridad de Api Rest
  app.use(helmet());
  //JSON en peticiones
  app.use(express.json());
  // URL Limpias y tambien enviar por query
  app.use(express.urlencoded({extended: true}));

  app.use('/static/tables',express.static('res/tables'))
  app.use('/static/products',express.static('res/products'))

  // Importar las rutas
  app.use('/api', require('./routes/main.route'));



  app.listen(port, () => {
    console.log(`Running in htpp://localhost:${port}`);
  });
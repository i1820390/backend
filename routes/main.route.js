const express = require('express');
const api = express.Router();
const path = require('path');
const multer = require('multer');
// PRODUCTOS
const locationUpload = path.join(__dirname, '../res/products');
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    console.log(locationUpload);
    cb(null, locationUpload)
  },
  filename: (req, file, cb) => {
    cb(null, `product-${Date.now()}${path.extname(file.originalname)}`)
  }
});

const upload = multer({storage: storage})

const locationUploadTable = path.join(__dirname, '../res/tables');
const storageTable = multer.diskStorage({
  destination: (req, file, cb) => {
    console.log(locationUploadTable);
    cb(null, locationUploadTable)
  },
  filename: (req, file, cb) => {
    cb(null, `table-${Date.now()}${path.extname(file.originalname)}`)
  }
});

const uploadTable = multer({storage: storageTable})

const Products = require('../controllers/product.controller');
api.post('/products', Products.create);
api.get('/products', Products.findAll)
api.post('/products-upload-photo', upload.single('photo'), Products.uploadPhoto);
api.delete('/products-delete', Products.drop);
api.put('/products-update', Products.put);

const Categories = require('../controllers/category.controller');
api.post('/categories', Categories.create);
api.get('/categories', Categories.findAll)

const Phones = require('../controllers/phone.controller');
api.get('/phones', Phones.getPhoneByUserId);

const Users = require('../controllers/user.controller');
api.post('/user', Users.create);
api.post('/login', Users.login);
api.post('/forgot-password', Users.forgotPassword);
api.post('/change-password', Users.changePassword);
api.get('/user-by', Users.findAll);
api.get('/users', Users.findAllInexact);
api.delete('/user-delete', Users.drop);
api.put('/user-update', Users.put);

const Tables = require('../controllers/table.controller');
api.get('/tables', Tables.findAll);
api.post('/table', Tables.create);
api.post('/table-upload-photo', uploadTable.single('tablePhoto'), Tables.upload);
api.delete('/table-delete', Tables.drop);
api.put('/table', Tables.put);


const Sales = require('../controllers/sale.controller');
api.post('/sale', Sales.create);
api.get('/sales', Sales.findAll);
api.put('/sale', Sales.update);
api.delete('/sale', Sales.drop);
api.get('/sales-report', Sales.findAllReport);

const SaleDetails = require('../controllers/sale-detail.controller');
api.post('/sale-detail', SaleDetails.create);
api.get('/sale-detail-by-sale', SaleDetails.findBySaleId);

const Distributions = require('../controllers/distribution.controller');
api.get('/distributions-by-table', Distributions.findByTable);
api.get('/distributions-by-sale', Distributions.findBySale);

const Fractions = require('../controllers/fraction.controller');
api.get('/fractions', Fractions.findAll);


module.exports = api;
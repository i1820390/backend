const config = require('../config/db.config')().db;
const Sequelize = require('sequelize');
const conection = new Sequelize(config.options.db, config.username, config.password, config.options);

create = (req, res) => {

  conection
  .authenticate()
  .then(() => {
    console.log(req.body);
    let sql = "SELECT * FROM public.fx_create_sale_details(:jsonbulk)";
    let parameters = {
      jsonbulk: JSON.stringify(req.body.jsonbulk)
    };
    let type = Sequelize.QueryTypes.SELECT;

    conection
    .query(sql, {replacements: parameters}, type)
    .spread(data => {
      res.status(200).json(data);
    })
    .catch(err => {
      console.log(err);
    })

  })
  .catch(err => {
    console.log(err);
  });
}

findBySaleId = async (req, res) => {
  try {
    conectionVerify = await conection.authenticate();
    let sql = "SELECT * FROM public.fx_get_sale_detail_by_sale(:sale_id)";
    let parameters = {
      sale_id: req.query.sale_id
    };
    let type = Sequelize.QueryTypes.SELECT;
    let result = await conection.query(sql, {replacements: parameters}, type);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
}

module.exports = {
  create,
  findBySaleId
}
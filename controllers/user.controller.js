const config = require('../config/db.config')().db;
const Sequelize = require('sequelize');
const conection = new Sequelize(config.options.db, config.username, config.password, config.options);
const md5 = require('blueimp-md5');

const EmailUtils = require('./utils/send-email.utils');
const Verifier = require('email-verifier');

const verifier = new Verifier('at_byAYG1ZB0bt4SDUDj1E2HAWas07Sa');
create = (req, res) => {
  conection
  .authenticate()
  .then(() => {
    console.log(req.body);
    let sql = "SELECT * FROM public.fx_create_users(:jsonbulk)";
    req.body.newUser.gender = req.body.newUser.gender === 'male';
    req.body.newUser.password = md5(req.body.newUser.password);
    let parameters = {
      jsonbulk: JSON.stringify([req.body.newUser])
    };
    let type = Sequelize.QueryTypes.SELECT;

    conection
    .query(sql, {replacements: parameters}, type)
    .spread(data => {
      if (data[0].fx_create_users) {
        res.status(200).json({error: false, message: 'Usuario creado satisfactoriamente.'});
      } else {
        res.status(200).json({error: true, message: 'El usuario ya existe, por favor ingrese otro usuario.'});
      }
    })
    .catch(err => {
      console.log(err);
    })

  })
  .catch(err => {
    console.log(err);
  });
}

login = (req, res) => {
  conection
  .authenticate()
  .then(() => {
    let sql = "SELECT * FROM public.fx_login(:username, :password, :withEmail)";
    let parameters = {
      username: req.body.username,
      password: req.body.password === null ? null : md5(req.body.password),
      withEmail: req.body.password === null
    };
    let type = Sequelize.QueryTypes.SELECT;

    conection
    .query(sql, {replacements: parameters}, type)
    .spread(data => {
      console.log(data);
      if (data.length) {
        res.status(200).json(data[0]);
      } else {
        res.status(200).json(null);
      }
    })
    .catch(err => {
      console.log(err);
    })

  })
  .catch(err => {
    console.log(err);
  });
}

forgotPassword = async (req, res) => {
  try {
    conectionVerify = await conection.authenticate();
    let sql = "SELECT * FROM fx_get_users_by_exact(:param)";
    let parameters = {
      param: req.body.email
    };
    let type = Sequelize.QueryTypes.SELECT;

    let result = await conection.query(sql, {replacements: parameters}, type);
    if (result[0].length) {
      let subject = "Confirmación de Reinicio de Contraseña";
      let text = "Ud., hace instantes a solicitado cambiar su contraseña, SINO FUE UD., por seguridad cambie su contraseña."
      let data = result[0][0];
      EmailUtils.sendMailGmail(result[0][0].email, subject,'', [], 'forgot-password', data);
      res.status(200).json({message: 'Se envio la confirmación de cambio de Contraseña.'});
    }
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
}

changePassword = async (req, res) => {
  try {
    conectionVerify = await conection.authenticate();
    let sql = "SELECT * FROM fx_update_password(:userId, :newPassword)";
    let parameters = {
      userId: req.body.userId,
      newPassword: md5(req.body.newPassword)
    };
    let type = Sequelize.QueryTypes.SELECT;

    let result = await conection.query(sql, {replacements: parameters}, type);
    res.status(200).json(result[0][0].fx_update_password);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
}


findAll = async (req, res) => {
  try {
    conectionVerify = await conection.authenticate();
    let sql = "SELECT * FROM fx_get_users_by_exact(:param)";
    let parameters = {
      param: req.query.param
    };
    let type = Sequelize.QueryTypes.SELECT;

    let result = await conection.query(sql, {replacements: parameters}, type);
    
    if (result[0].length === 0 && req.query.param.includes('@')) {
      verifier.verify(req.query.param, (err, data) => {
        if (err) throw err;
        
        res.status(200).json({result: result[0], verifier: data});
      }) 
    } else {
      res.status(200).json(result[0]);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
}

findAllInexact = async (req, res) => {
  try {
    conectionVerify = await conection.authenticate();
    let sql = "SELECT * FROM fx_get_users_by_inexact(:param)";
    let parameters = {
      param: '%'
    };
    let type = Sequelize.QueryTypes.SELECT;

    let result = await conection.query(sql, {replacements: parameters}, type);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
}


drop = async(req, res) => {
  try {
    let conectionVer = conection.authenticate();
    let sql = 'SELECT * FROM fx_delete_users(:id)';
    let parameters = {
      id: req.query.id
    };
    let type = Sequelize.QueryTypes.SELECT;
    let result = await conection.query(sql, {replacements: parameters}, type);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(500).json({message: error});
  }
}

put = async (req, res) => {
  
  try {
    let conectionVer = conection.authenticate();
    let sql = 'SELECT * FROM fx_update_user(:jsonbulk)';
    let parameters = {
      jsonbulk: JSON.stringify([req.body.newUser])
    };
    let type = Sequelize.QueryTypes.SELECT;
    let result = await conection.query(sql, {replacements: parameters}, type);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(500).json({message: error});
  }
}


module.exports = {
  create,
  login,
  forgotPassword,
  changePassword,
  findAll,
  findAllInexact,
  drop,
  put
}
const config = require('../config/db.config')().db;
const Sequelize = require('sequelize');
const conection = new Sequelize(config.options.db, config.username, config.password, config.options);

create = (req, res) => {
  conection
  .authenticate()
  .then(() => {
    let sql = "SELECT * FROM public.fx_create_products(:jsonbulk)";
    let parameters = {
      jsonbulk: JSON.stringify([req.body])
    };
    let type = Sequelize.QueryTypes.SELECT;

    conection
    .query(sql, {replacements: parameters}, type)
    .spread(data => {
      res.status(200).json(data);
    })
    .catch(err => {
      console.log(err);
    });
  })
  .catch(err => {
    console.log(err);
  });
}

findAll = (req, res) => {
  conection
  .authenticate()
  .then(() => {
    let sql = "SELECT * FROM public.fx_get_products()";
    let parameters = {};
    let type = Sequelize.QueryTypes.SELECT;

    conection
    .query(sql, {replacements: parameters}, type)
    .spread(data => {
      res.status(200).json(data.reverse());
    })
    .catch(err => {
      console.log(err);
    })

  })
  .catch(err => {
    console.log(err);
  });
}

uploadPhoto = (req, res) => {
  res.status(200).json({message: 'upload success', file: req.file});
}

drop = async(req, res) => {
  try {
    let conectionVer = conection.authenticate();
    let sql = 'SELECT * FROM fx_delete_products(:id)';
    let parameters = {
      id: req.query.id
    };
    let type = Sequelize.QueryTypes.SELECT;
    let result = await conection.query(sql, {replacements: parameters}, type);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(500).json({message: error});
  }
}

put = async(req, res) => {
  try {
    let conectionVer = conection.authenticate();
    let sql = 'SELECT * FROM fx_update_product(:jsonbulk)';
    let parameters = {
      jsonbulk: JSON.stringify([req.body])
    };
    let type = conection.QueryTypes.SELECT;
    let result = await conection.query(sql, {replacements: parameters}, type);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(500).json({message: error});
  }
}

module.exports = {
  create,
  findAll,
  uploadPhoto,
  drop,
  put
}

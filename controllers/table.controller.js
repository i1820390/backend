const config = require('../config/db.config')().db;
const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.options.db, config.username, config.password, config.options);

create = async (req, res) => {
  try {
    let conection = sequelize.authenticate();
    let sql = 'SELECT * FROM fx_create_tables(:jsonbulk)';
    let parameters = {
      jsonbulk: JSON.stringify(req.body.jsonbulk)
    };
    let type = Sequelize.QueryTypes.SELECT;
    let result = await sequelize.query(sql, {replacements: parameters}, type);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(500).json({message: error});
  }
}

findAll = async (req, res) => {
  try {
    let conection = sequelize.authenticate();
    let sql = 'SELECT * FROM fx_get_tables()';
    let parameters = {};
    let type = Sequelize.QueryTypes.SELECT;
    let result = await sequelize.query(sql, {replacements: parameters}, type);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(500).json({message: error});
  }
}

upload = (req, res) => {
  console.log(req.file);
  res.status(200).json({file: req.file});
}


drop = async(req, res) => {
  try {
    let conection = sequelize.authenticate();
    let sql = 'SELECT * FROM fx_delete_tables(:id)';
    let parameters = {
      id: req.query.id
    };
    let type = Sequelize.QueryTypes.SELECT;
    let result = await sequelize.query(sql, {replacements: parameters}, type);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(500).json({message: error});
  }
}

put = async (req, res) => {
  try {
    let conection = sequelize.authenticate();
    let sql = 'SELECT * FROM fx_update_table(:jsonbulk)';
    let parameters = {
      jsonbulk: JSON.stringify(req.body.jsonbulk)
    };
    let type = Sequelize.QueryTypes.SELECT;
    let result = await sequelize.query(sql, {replacements: parameters}, type);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(500).json({message: error});
  }
}


module.exports = {
  findAll,
  upload,
  create,
  drop,
  put
}
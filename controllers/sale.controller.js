const config = require('../config/db.config')().db;
const Sequelize = require('sequelize');
const conection = new Sequelize(config.options.db, config.username, config.password, config.options);
const QRcode = require('qrcode');
const path = require('path');
const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
const moment = require('moment');


const Credentials = require('../config/credential.config');


const emailEntity = 'chifachaufan@gmail.com';

const pdf = require('html-pdf');

createQRURL = async text => {
  try {
    return await QRcode.toDataURL(text.toString());
  } catch (err) {
    console.log('aqui');
    console.error(err)
  }
}

createQRFILE = async text => {
  try {
    return await QRcode.toFile(`./res/qrs/${text}.png`, text.toString());
  } catch (err) {
    console.log('aqui');
    console.error(err)
  }
}

sendMailGmail = async (qr, data) => {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: Credentials.userGmail.username,
      pass: Credentials.userGmail.password
    }
  });

  var handlebarsOptions = {
    viewEngine: {
        extName: '.handlebars',
        partialsDir: path.resolve('./templates/'),
        layoutsDir: path.resolve('./templates/'),
        defaultLayout: '',
    },
    viewPath: path.resolve('./templates/'),
    extName: '.handlebars'
};

  transporter.use('compile', hbs(handlebarsOptions))
  let images = [
    {
      filename: 'logo.png',
      path: './res/logo.png',
      cid: 'logo.logo1'
    }, 
    {
      filename: `${data.sale_id}.png`,
      path: `./res/qrs/${data.sale_id}.png`,
      cid: `qr.${data.sale_id}.png`
    }
  ];
  data.detail.map(i => {
    i._photo = `./res/products/${i.photo}`;
    images.push({
      filename: `${i.photo}`,
      path: i._photo,
      cid: i.photo
    });
  });

  const mailOptions = {
    from: data.sender, // sender address
    // to: data.email, // list of receivers
    to: data.customer_email, // list of receivers
    subject: 'Verificación de Reserva', // Subject line
    text: 'Hola es una verificacion',
    // html: '<img src="cid:logo.logo1"/><p>Tu pedido fue</p>',// plain text body
    attachments: images,
    template: 'done-reservation',
    context: {
      data: data
    }
  };
  let a = await transporter.sendMail(mailOptions);
  return {message: 'Se envio el correo de reservación'};

}

create = async (req, res) => {
  try {
    conectionVerify = await conection.authenticate();
    let sql = "SELECT * FROM public.fx_create_sales(:jsonbulk)";
    let parameters = {
      jsonbulk: JSON.stringify([req.body.cabecera])
    };
    let type = Sequelize.QueryTypes.SELECT;

    let result = await conection.query(sql, {replacements: parameters}, type);
    let newSaleId = result[0][0].id;
    req.body.cabecera.fractions.map(i => {
      i.sale_id = newSaleId;
      i.table_id = req.body.cabecera.table_id;
    });
    console.log(req.body.cabecera.fractions);
    let sqlFractions = "SELECT * FROM public.fx_create_fractions(:jsonbulk)";
    let paramsFractions = {
      jsonbulk: JSON.stringify(req.body.cabecera.fractions)
    };
    let resultFractions = await conection.query(sqlFractions, {replacements: paramsFractions}, type);
    console.log(resultFractions[0]);
    req.body.detalle.map(i => {
      i.sale_id = newSaleId;
    });
    let sqlDetail = "SELECT * FROM public.fx_create_sale_details(:jsonbulk)";
    let parametersDetail = {
      jsonbulk: JSON.stringify(req.body.detalle)
    };

    let resultDetail = await conection.query(sqlDetail, {replacements: parametersDetail}, type);
    if (resultDetail[0][0].fx_create_sale_details) {
      let qrGenerated = await createQRFILE(result[0][0].id);
      let response_qrcode = {qrcode: await createQRURL(result[0][0].id)};
      let data = {
        username: '',
        customer_email: req.body.cabecera.customer_email,
        sale_id: result[0][0].id,
        email: result[0][0].email,
        // create_at: new Date(),
        create_at: moment().format('DD/MM/YYYY HH:MM'),
        date_reservation: moment(req.body.cabecera.date_reservation).format('DD/MM/YYYY'),
        init_fraction: req.body.cabecera.initFraction,
        finish_fraction: req.body.cabecera.finishFraction,

        table_name: req.body.cabecera.table_name,
        detail: req.body.detalle,
        qr: qrGenerated,
        subtotal: req.body.cabecera.total_amount,
        disacount: 0,
        total: req.body.cabecera.total_amount,
        sender: emailEntity
      };
      // Poner despues de generacion de email
      
      let response = sendMailGmail(qrGenerated, data);
      response.qrcode = qrGenerated;
      res.status(200).json(response_qrcode);
    } else {
      res.status(200).json({message: 'Error en inserción'});
    }

  } catch (error) {
    console.log(error);   
    res.status(500).json(error); 
  }
}

findAll = (req, res) => {
  conection
  .authenticate()
  .then(() => {
    let sql = "SELECT * FROM public.fx_get_sales(:customer_id)";
    let parameters = {
      customer_id: req.query.customer_id ? parseInt(req.query.customer_id) : 0
    };
    let type = Sequelize.QueryTypes.SELECT;

    conection
    .query(sql, {replacements: parameters}, type)
    .spread(data => {
      res.status(200).json(data);
    })
    .catch(error => {
      console.log(error);
      res.status(500).json(error);
    });
    
  })
  .catch(error => {
    console.log(err);
    res.status(500).json(error);
  });
}

update = async (req, res) => {
  try {
    conectionVerify = await conection.authenticate();
    let sql = "SELECT * FROM public.fx_update_sale(:jsonbulk)";
    let parameters = {
      jsonbulk: JSON.stringify([req.body.cabecera])
    };
    let type = Sequelize.QueryTypes.SELECT;
    let result = await conection.query(sql, {replacements: parameters}, type);
    console.log(result[0][0].fx_fx_update_sale);
    let sqlDetail = "SELECT * FROM public.fx_update_sale_details(:sale_id, :jsonbulk)";
    req.body.detalle.map(i => i.sale_id = req.body.cabecera.id);
    let parametersDetail = {
      sale_id: req.body.cabecera.id,
      jsonbulk: JSON.stringify(req.body.detalle)
    };
    let resultDetail = await conection.query(sqlDetail, {replacements: parametersDetail}, type);
    let response_qrcode = {qrcode: await createQRURL(req.body.cabecera.id)};

    let data = {
      username: '',
      customer_email: req.body.cabecera.customer_email,
      sale_id: req.body.cabecera.id,
      email: req.body.cabecera.customer_email,
      create_at: new Date(),
      table_name: req.body.cabecera.table_name,
      detail: req.body.detalle,
      qr: response_qrcode,
      subtotal: req.body.cabecera.total_amount,
      disacount: 0,
      total: req.body.cabecera.total_amount,
      sender: emailEntity
    };
    // Poner despues de generacion de email
    
    let response = sendMailGmail(response_qrcode, data);

    res.status(200).json(response_qrcode);
  } catch (error) {
    console.log(error);
    res.status(500).json({message: error});
  }
}

drop = async ( req, res) => {
  try {
    conectionVerify = await conection.authenticate();
    let sql = "SELECT * FROM public.fx_delete_sale(:sale_id)";
    let parameters = {
      sale_id: req.query.sale_id
    };
    let type = Sequelize.QueryTypes.SELECT;
    let result = await conection.query(sql, {replacements: parameters}, type);
    console.log(result);
    res.status(200).json(result[0]);
  } catch (error) {
    res.status(500).json({message: error});
  }
}

var nunjucks = require('nunjucks')
nunjucks.configure(path.join(__dirname, '/templates'));


findAllReport = (req, res) => {
  conection
  .authenticate()
  .then(() => {
    let sql = "SELECT * FROM public.fx_get_sales()";
    let parameters = {
    };
    let type = Sequelize.QueryTypes.SELECT;

    conection
    .query(sql, {replacements: parameters}, type)
    .spread(data => {

      let dataToReport = {
        data: data,
        total_sales: data.reduce((memo, i) => { return memo = memo + parseFloat(i.total_amount);  },0)
      } 
      
      html = nunjucks.render('sale.report.html', dataToReport);
      let options = {
        base: 'file:///'+path.join(__dirname,'templates')
      }
      pdf.create(html, options).toFile('./templates/report.pdf', (err, result) => {
        console.log(result);
        res.status(200).json({data: data, file: result});
      });
    })
    .catch(error => {
      console.log(error);
      res.status(500).json(error);
    });
    
  })
  .catch(error => {
    console.log(err);
    res.status(500).json(error);
  });
}

module.exports = {
  create,
  findAll,
  update,
  drop,
  findAllReport
}
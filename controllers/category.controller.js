const config = require('../config/db.config')().db;
const Sequelize = require('sequelize');
const conection = new Sequelize(config.options.db, config.username, config.password, config.options);

create = (req, res) => {
  conection
  .authenticate()
  .then(() => {
    let sql = "SELECT * FROM public.fx_create_categories(:jsonbulk)";
    let parameters = {
      jsonbulk: req.body.jsonbulk
    };
    let type = Sequelize.QueryTypes.SELECT;

    conection
    .query(sql, {replacements: parameters}, type)
    .spread(data => {
      res.status(200).json(data);
    })
    .catch(err => {
      console.log(err);
    })

  })
  .catch(err => {
    console.log(err);
  });
}

findAll = (req, res) => {
  conection
  .authenticate()
  .then(() => {
    let sql = "SELECT * FROM public.fx_get_categories()";
    let parameters = {};
    let type = Sequelize.QueryTypes.SELECT;

    conection
    .query(sql, {replacements: parameters}, type)
    .spread(data => {
      res.status(200).json(data);
    })
    .catch(err => {
      console.log(err);
    })

  })
  .catch(err => {
    console.log(err);
  });
}

module.exports = {
  create,
  findAll
}

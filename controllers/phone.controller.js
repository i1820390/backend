const config = require('../config/db.config')().db;
const Sequelize = require('sequelize');
const conection = new Sequelize(config.options.db, config.username, config.password, config.options);

getPhoneByUserId = (req, res) => {
	conection
	.authenticate()
	.then(() => {
		let sql = "select * from public.fx_get_phones(:user_id)";
		let params = {
			user_id: parseInt(req.query.user_id)
		};
		let type = conection.QueryTypes.SELECT;

		conection
		.query(sql, {replacements: params}, type)
		.spread(data => {
			res.status(200).json(data);
		})
		.catch((err) => {
			console.log(err);
		})

	})
	.catch((err) => {
			console.log(err);
	});
}

module.exports = {
	getPhoneByUserId
}
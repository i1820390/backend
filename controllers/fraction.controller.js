const config = require('../config/db.config')().db;
const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.options.db, config.username, config.password, config.options);


findAll = async (req, res) => {
  try {
    let conection = sequelize.authenticate();
    let sql = 'SELECT * FROM fx_get_fractions()';
    let parameters = {};
    let type = Sequelize.QueryTypes.SELECT;
    let result = await sequelize.query(sql, {replacements: parameters}, type);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(500).json({message: error});
  }
}

module.exports = {
  findAll
}
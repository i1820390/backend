const config = require('../config/db.config')().db;
const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.options.db, config.username, config.password, config.options);


findByTable = async (req, res) => {
  try {
    let conection = sequelize.authenticate();
    let sql = 'SELECT * FROM fx_get_distributions_of_table(:table_id, :date_reservation::date)';
    let parameters = {
      table_id: parseInt(req.query.table_id),
      date_reservation: req.query.date_reservation
    };
    let type = Sequelize.QueryTypes.SELECT;
    let result = await sequelize.query(sql, {replacements: parameters}, type);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(500).json({message: error});
  }
}


findBySale = async(req, res)=> {
  try {
    let conection = sequelize.authenticate();
    let sql = 'SELECT * FROM fx_get_distributions_by_sale(:sale_id)';
    let parameters = {
      sale_id: parseInt(req.query.sale_id)
    };
    let type = Sequelize.QueryTypes.SELECT;
    let result = await sequelize.query(sql, {replacements: parameters}, type);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(500).json({message: error});
  }
}


module.exports = {
  findByTable,
  findBySale
}
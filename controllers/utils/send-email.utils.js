const path = require('path');
const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars')

const Credentials = require('../../config/credential.config');

sendMailGmail = async (destiny, subject, text, attachments, template, data) => {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: Credentials.userGmail.username,
      pass: Credentials.userGmail.password
    }
  });

  var handlebarsOptions = {
    viewEngine: {
        extName: '.handlebars',
        partialsDir: path.resolve('./templates/'),
        layoutsDir: path.resolve('./templates/'),
        defaultLayout: '',
    },
    viewPath: path.resolve('./templates/'),
    extName: '.handlebars'
  };

  transporter.use('compile', hbs(handlebarsOptions))
  let _attachments = [
    {
      filename: 'logo.png',
      path: './res/logo.png',
      cid: 'cid.logo'
    }
  ];

  _attachments = [...attachments, attachments];

  const mailOptions = {
    from: 'chifachaufan@gmail.com', // sender address
    to: destiny, // list of receivers
    subject: subject, // Subject line
    text: text,
    attachments: _attachments,
    template: template,
    context: {
      data: data
    }
  };
  let a = await transporter.sendMail(mailOptions);
  return {message: 'Se envio el correo electrónico'};

}

module.exports = {
  sendMailGmail
}
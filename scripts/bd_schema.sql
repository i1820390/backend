--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

-- Started on 2020-02-13 08:48:22

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 707 (class 1247 OID 24937)
-- Name: status_t; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.status_t AS ENUM (
    'reserved',
    'inProcess',
    'attended',
    'cancelled'
);


ALTER TYPE public.status_t OWNER TO postgres;

--
-- TOC entry 674 (class 1247 OID 24859)
-- Name: user_types_t; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.user_types_t AS ENUM (
    'customer',
    'admin',
    'user'
);


ALTER TYPE public.user_types_t OWNER TO postgres;

--
-- TOC entry 659 (class 1247 OID 24578)
-- Name: usertypes_t; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.usertypes_t AS ENUM (
    'customer',
    'admin',
    'user'
);


ALTER TYPE public.usertypes_t OWNER TO postgres;

--
-- TOC entry 253 (class 1255 OID 34259)
-- Name: fx_create_categories(jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_create_categories(jsonb) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_json	alias for $1;	
BEGIN
	
	drop table if exists tmp_categories;
	create temporary table tmp_categories as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			name character varying,
			description character varying
	);
	
	insert 	into categories(name, description)
	select	*
	from	tmp_categories a;
	
	return 	true;
	
END; $_$;


ALTER FUNCTION public.fx_create_categories(jsonb) OWNER TO postgres;

--
-- TOC entry 249 (class 1255 OID 34039)
-- Name: fx_create_fractions(jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_create_fractions(jsonb) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_json				alias for $1;
BEGIN
	drop table if exists tmp_fractions;
	create temporary table tmp_fractions as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			id integer,
			sale_id integer,
			table_id integer
	);
	
	insert 	into distributions(sale_id, table_id, fraction_id, create_by)
	select	a.sale_id,
			a.table_id,
			a.id,
			0
	from	tmp_fractions a;
	
	return 	true;
	
END; $_$;


ALTER FUNCTION public.fx_create_fractions(jsonb) OWNER TO postgres;

--
-- TOC entry 252 (class 1255 OID 34258)
-- Name: fx_create_products(jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_create_products(jsonb) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_json	alias for $1;	
BEGIN
	
	drop table if exists tmp_products;
	create temporary table tmp_products as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			name character varying,
			description character varying,
			category_id integer,
			unit_price numeric(13,2),
			photo character varying
	);
	
	insert 	into products(name, description, category_id, unit_price, photo)
	select	*
	from	tmp_products a;
	
	return 	true;
	
END; $_$;


ALTER FUNCTION public.fx_create_products(jsonb) OWNER TO postgres;

--
-- TOC entry 260 (class 1255 OID 34266)
-- Name: fx_create_sale_details(jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_create_sale_details(jsonb) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_json	alias for $1;
BEGIN
	
	drop table if exists tmp_sale_details;
	create temporary table tmp_sale_details as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			sale_id integer,
			product_id integer,
			unit_price numeric(13,2),
			quantity integer,
			price numeric(13,2)
	);
	
	insert 	into sale_details(sale_id, product_id, unit_price, quantity, price)
	select	*
	from	tmp_sale_details a;
	
	return 	true;
	
END; $_$;


ALTER FUNCTION public.fx_create_sale_details(jsonb) OWNER TO postgres;

--
-- TOC entry 258 (class 1255 OID 34264)
-- Name: fx_create_sales(jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_create_sales(jsonb) RETURNS TABLE(id integer, email character varying)
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_json	alias for $1;
BEGIN
	
	drop table if exists tmp_sales;
	create temporary table tmp_sales as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			total_amount numeric(13,2),
			customer_id integer,
			table_id integer,
			user_id integer,
			date_reservation date,
			time_reservation time,
			persons_quantity integer
	);
	
	insert 	into sales(total_amount, customer_id, table_id, user_id, date_reservation, time_reservation, persons_quantity)
	select	*
	from	tmp_sales a;
	
	return 	query
	select	a.id,
			b.email
	from	sales a
			inner join users b
			on a.customer_id = b.id 
	order 	by modify_at desc
	limit	1
	;
	
END; $_$;


ALTER FUNCTION public.fx_create_sales(jsonb) OWNER TO postgres;

--
-- TOC entry 272 (class 1255 OID 33824)
-- Name: fx_create_tables(jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_create_tables(jsonb) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_json	alias for $1;	
BEGIN
	
	drop table if exists tmp_tables;
	create temporary table tmp_tables as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			name character varying,
			number integer,
			photo character varying
	);
	
	insert 	into tables(name, number, photo)
	select	*
	from	tmp_tables a;
	
	return 	true;
	
END; $_$;


ALTER FUNCTION public.fx_create_tables(jsonb) OWNER TO postgres;

--
-- TOC entry 251 (class 1255 OID 34257)
-- Name: fx_create_users(jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_create_users(jsonb) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_json	alias for $1;	
	
	email_unique 		character varying;
	username_unique 	character varying;
BEGIN
	
	drop table if exists tmp_users;
	create temporary table tmp_users as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			names character varying,
			lastnames character varying,
			gender boolean,
			user_type user_types_t,
			email character varying,
			username character varying,
			password character varying
	);
	
	select	username,
			email
	into	username_unique,
			email_unique
	from	tmp_users
	limit 	1;
	
	if exists (select * from users where username = username_unique or email = email_unique) then
		return false;
	else
		insert 	into users(names, lastnames, gender, user_type, email, username, password)
		select	*
		from	tmp_users a;
		
		return true;
	end if;

END; $_$;


ALTER FUNCTION public.fx_create_users(jsonb) OWNER TO postgres;

--
-- TOC entry 275 (class 1255 OID 33827)
-- Name: fx_delete_products(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_delete_products(integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_id	alias for $1;	
BEGIN
	delete 	from products
	where	id = p_id;
	
	return 	true;
	
END; $_$;


ALTER FUNCTION public.fx_delete_products(integer) OWNER TO postgres;

--
-- TOC entry 266 (class 1255 OID 34275)
-- Name: fx_delete_sale(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_delete_sale(integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_sale_id	alias for $1;
BEGIN
	
	delete	from sales
	where	id = p_sale_id;
	
	delete	from distributions
	where	sale_id = p_sale_id;
	
	delete from sale_details
	where	sale_id = p_sale_id;
	
	return	true;
	
END; $_$;


ALTER FUNCTION public.fx_delete_sale(integer) OWNER TO postgres;

--
-- TOC entry 273 (class 1255 OID 33825)
-- Name: fx_delete_tables(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_delete_tables(integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_id	alias for $1;	
BEGIN
	delete 	from tables
	where	id = p_id;
	
	return 	true;
	
END; $_$;


ALTER FUNCTION public.fx_delete_tables(integer) OWNER TO postgres;

--
-- TOC entry 274 (class 1255 OID 33826)
-- Name: fx_delete_users(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_delete_users(integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_id	alias for $1;	
BEGIN
	delete 	from users
	where	id = p_id;
	
	return 	true;
	
END; $_$;


ALTER FUNCTION public.fx_delete_users(integer) OWNER TO postgres;

--
-- TOC entry 254 (class 1255 OID 34260)
-- Name: fx_get_categories(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_get_categories() RETURNS TABLE(id integer, name character varying, description character varying)
    LANGUAGE plpgsql
    AS $$

BEGIN
	return 	query
	select	* 
	from	categories;
	
END; $$;


ALTER FUNCTION public.fx_get_categories() OWNER TO postgres;

--
-- TOC entry 250 (class 1255 OID 34050)
-- Name: fx_get_distributions_by_sale(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_get_distributions_by_sale(integer) RETURNS TABLE(sale_id integer, table_id integer, fraction_id integer, create_by integer, create_at abstime)
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_sale_id				alias for $1;
BEGIN
	
	return query
	select 	a.*
	from	distributions a
	where 	a.sale_id = p_sale_id;
	
END; $_$;


ALTER FUNCTION public.fx_get_distributions_by_sale(integer) OWNER TO postgres;

--
-- TOC entry 248 (class 1255 OID 34003)
-- Name: fx_get_distributions_of_table(integer, date); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_get_distributions_of_table(integer, date) RETURNS TABLE(sale_id integer, table_id integer, fraction_id integer, create_by integer, create_at abstime, sale_date date)
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_table_id				alias for $1;
	p_date_reservation		alias for $2;
BEGIN
	
	return query
	select 	a.*,
			b.date_reservation::date
	from	distributions a
			inner join sales b
			on a.sale_id = b.id
	where 	a.table_id = p_table_id
	and		b.date_reservation::date = p_date_reservation;
END; $_$;


ALTER FUNCTION public.fx_get_distributions_of_table(integer, date) OWNER TO postgres;

--
-- TOC entry 256 (class 1255 OID 34262)
-- Name: fx_get_document_types(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_get_document_types(integer) RETURNS TABLE(id integer, user_id integer, document_type character varying, document_number character varying)
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_user_id	alias for $1;
BEGIN
	return 	query
	select	* 
	from	document_types
	where	user_id = p_user_id;
	
END; $_$;


ALTER FUNCTION public.fx_get_document_types(integer) OWNER TO postgres;

--
-- TOC entry 247 (class 1255 OID 34001)
-- Name: fx_get_fractions(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_get_fractions() RETURNS TABLE(id integer, init time without time zone, finish time without time zone)
    LANGUAGE plpgsql
    AS $$
DECLARE
	
BEGIN
	
	return query
	select 	*
	from	fractions;
END; $$;


ALTER FUNCTION public.fx_get_fractions() OWNER TO postgres;

--
-- TOC entry 257 (class 1255 OID 34263)
-- Name: fx_get_phones(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_get_phones(integer) RETURNS TABLE(id integer, user_id integer, phone_type character varying, phone_number character varying)
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_user_id	alias for $1;
BEGIN
	return 	query
	select	* 
	from	phones p
	where	p.user_id = p_user_id;
	
END; $_$;


ALTER FUNCTION public.fx_get_phones(integer) OWNER TO postgres;

--
-- TOC entry 255 (class 1255 OID 34261)
-- Name: fx_get_products(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_get_products() RETURNS TABLE(id integer, name character varying, description character varying, category_id integer, unit_price numeric, photo character varying, status boolean, create_at abstime)
    LANGUAGE plpgsql
    AS $$

BEGIN
	return 	query
	select	* 
	from	products;
	
END; $$;


ALTER FUNCTION public.fx_get_products() OWNER TO postgres;

--
-- TOC entry 262 (class 1255 OID 34268)
-- Name: fx_get_sale_detail_by_sale(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_get_sale_detail_by_sale(integer) RETURNS TABLE(id integer, sale_id integer, product_id integer, unit_price numeric, quantity integer, price numeric)
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_sale_id	alias for $1;
BEGIN
	
	return 	query
	select 	*
	from	sale_details a
	where	a.sale_id = p_sale_id;
	
END; $_$;


ALTER FUNCTION public.fx_get_sale_detail_by_sale(integer) OWNER TO postgres;

--
-- TOC entry 259 (class 1255 OID 34265)
-- Name: fx_get_sales(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_get_sales(integer) RETURNS TABLE(id integer, total_amount numeric, disacount numeric, customer_id integer, table_id integer, user_id integer, status public.status_t, date_reservation date, time_reservation time without time zone, persons_quantity integer, create_at abstime, user_modify integer, modify_at abstime, names character varying, lastnames character varying, username character varying)
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_customer	alias for $1;
BEGIN
	
	if p_customer != 0 then
		return 	query
		select 	a.*,
				b.names,
				b.lastnames,
				b.username
		from	sales a
				inner join users b
					on a.customer_id = b.id
		where	a.customer_id = p_customer
		and		a.status = 'reserved';
	else
		return 	query
		select 	a.*,
				b.names,
				b.lastnames,
				b.username
		from	sales a
				inner join users b
					on a.customer_id = b.id
		where	a.status = 'reserved';
	end if;

END; $_$;


ALTER FUNCTION public.fx_get_sales(integer) OWNER TO postgres;

--
-- TOC entry 261 (class 1255 OID 34267)
-- Name: fx_get_tables(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_get_tables() RETURNS TABLE(id integer, name character varying, number integer, photo character varying, create_at abstime)
    LANGUAGE plpgsql
    AS $$
DECLARE
	
BEGIN
	
	return 	query
	select	*
	from 	tables;
	
END; $$;


ALTER FUNCTION public.fx_get_tables() OWNER TO postgres;

--
-- TOC entry 267 (class 1255 OID 25680)
-- Name: fx_get_tables(jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_get_tables(jsonb) RETURNS TABLE(id integer, name character varying, number integer, photo character varying, create_at abstime)
    LANGUAGE plpgsql
    AS $$
DECLARE
	
BEGIN
	
	return 	query
	select	*
	from 	tables;
	
END; $$;


ALTER FUNCTION public.fx_get_tables(jsonb) OWNER TO postgres;

--
-- TOC entry 269 (class 1255 OID 33455)
-- Name: fx_get_users_by_exact(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_get_users_by_exact(character varying) RETURNS TABLE(id integer, names character varying, lastnames character varying, gender boolean, user_type public.user_types_t, email character varying, username character varying, password character varying, create_at abstime)
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_param	alias for $1;
BEGIN
	
	return 	query
	select	*
	from	users a
	where	a.names = trim(p_param)
	or		a.lastnames = trim(p_param)
	or		a.username = trim(p_param)
	or		a.email = trim(p_param)
	;
	
END; $_$;


ALTER FUNCTION public.fx_get_users_by_exact(character varying) OWNER TO postgres;

--
-- TOC entry 268 (class 1255 OID 33454)
-- Name: fx_get_users_by_inexact(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_get_users_by_inexact(character varying) RETURNS TABLE(id integer, names character varying, lastnames character varying, gender boolean, user_type public.user_types_t, email character varying, username character varying, password character varying, create_at abstime)
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_param	alias for $1;
BEGIN
	
	return 	query
	select	*
	from	users a
	where	a.names ilike '%'||trim(p_param)||'%'
	or		a.lastnames ilike '%'||trim(p_param)||'%'
	or		a.username ilike '%'||trim(p_param)||'%'
	or		a.email ilike '%'||trim(p_param)||'%'
	;
	
END; $_$;


ALTER FUNCTION public.fx_get_users_by_inexact(character varying) OWNER TO postgres;

--
-- TOC entry 271 (class 1255 OID 33807)
-- Name: fx_login(character, character, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_login(character, character, boolean) RETURNS TABLE(id integer, names character varying, lastnames character varying, gender boolean, user_type public.user_types_t, email character varying, username character varying, password character varying, create_at abstime)
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_username	alias for $1;
	p_password	alias for $2;
	p_with_email	alias for $3;
BEGIN
	if p_with_email then
		return 	query
		select 	*
		from	users a
		where	a.email = p_username;
	else 
	
		return 	query
		select 	* 
		from 	users a
		where	a.username = p_username
		and 	a.password = p_password;
	end if;
	
END; $_$;


ALTER FUNCTION public.fx_login(character, character, boolean) OWNER TO postgres;

--
-- TOC entry 270 (class 1255 OID 33456)
-- Name: fx_update_password(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_update_password(integer, character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_user_id	alias for $1;
	p_new_password	alias for $2;
BEGIN
	
	update	users
	set		password = p_new_password
	where	id = p_user_id;
	
	
	return 	true;
	
END; $_$;


ALTER FUNCTION public.fx_update_password(integer, character varying) OWNER TO postgres;

--
-- TOC entry 276 (class 1255 OID 33823)
-- Name: fx_update_product(jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_update_product(jsonb) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_json	alias for $1;
BEGIN

	drop table if exists tmp_products;
	create temporary table tmp_products as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			id integer,
			name character varying,
			description character varying,
			category_id integer ,
			unit_price numeric(13,2) ,
			photo character varying,
			status boolean
	);
	
	update	products b
	set		name = a.name,
			description = a.description,
			category_id = a.category_id,
			unit_price = a.unit_price,
			photo = a.photo,
			status = a.status
	from	tmp_products a
	where	b.id = a.id;
	
	return 	true;
		
END; $_$;


ALTER FUNCTION public.fx_update_product(jsonb) OWNER TO postgres;

--
-- TOC entry 263 (class 1255 OID 34269)
-- Name: fx_update_sale(jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_update_sale(jsonb) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_json	alias for $1;
BEGIN

	drop table if exists tmp_sales;
	create temporary table tmp_sales as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			id integer,
			total_amount numeric(13,2),
			customer_id integer,
			table_id integer,
			user_id integer,
			date_reservation date,
			time_reservation time,
			persons_quantity integer
	);
	
	update	sales b
	set	table_id = a.table_id,
		date_reservation = a.date_reservation,
		time_reservation = a.time_reservation,
		persons_quantity = a.persons_quantity,
		total_amount = a.total_amount
	from	tmp_sales a
	where	b.id = a.id;
	
	return 	true;
	
	
END; $_$;


ALTER FUNCTION public.fx_update_sale(jsonb) OWNER TO postgres;

--
-- TOC entry 264 (class 1255 OID 34270)
-- Name: fx_update_sale_details(integer, jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_update_sale_details(integer, jsonb) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_sale_id	alias for $1;
	p_json		alias for $2;
BEGIN
	
	drop table if exists tmp_sale_details;
	create temporary table tmp_sale_details as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			sale_id integer,
			product_id integer,
			unit_price numeric(13,2),
			quantity integer,
			price numeric(13,2)
	);

	-- eliminar e insertar
	delete 	from sale_details
	where	sale_id = p_sale_id;
	
	insert 	into sale_details(sale_id, product_id, unit_price, quantity, price)
	select	*
	from	tmp_sale_details a;
	
	return 	true;
	
END; $_$;


ALTER FUNCTION public.fx_update_sale_details(integer, jsonb) OWNER TO postgres;

--
-- TOC entry 277 (class 1255 OID 33822)
-- Name: fx_update_table(jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_update_table(jsonb) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_json	alias for $1;
BEGIN

	drop table if exists tmp_tables;
	create temporary table tmp_tables as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			id integer,
			name character varying,
			number integer,
			photo character varying
	);
	
	update	tables b
	set	name = a.name,
		number = a.number,
		photo = a.photo
	from	tmp_tables a
	where	b.id = a.id;
	
	return 	true;
	
END; $_$;


ALTER FUNCTION public.fx_update_table(jsonb) OWNER TO postgres;

--
-- TOC entry 265 (class 1255 OID 34272)
-- Name: fx_update_tables(jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_update_tables(jsonb) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_json		alias for $1;
BEGIN

	drop table if exists tmp_tables;
	create temporary table tmp_tables as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			id integer,
			name character varying,
			number integer,
			photo character varying
	);

	-- eliminar e insertar
	update	tables b
	set	name = a.name,
		number = a.number,
		photo = a.photo,
		create_at = now()
	from	tmp_tables a
	where	b.id = a.id;
	
	return 	true;
	
END; $_$;


ALTER FUNCTION public.fx_update_tables(jsonb) OWNER TO postgres;

--
-- TOC entry 278 (class 1255 OID 33739)
-- Name: fx_update_user(jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fx_update_user(jsonb) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
	p_json	alias for $1;
	
	l_user_id	integer;
	BEGIN

	drop table if exists tmp_users;
	create temporary table tmp_users as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			id integer,
			names character varying ,
			lastnames character varying ,
			gender boolean,
			phone character varying,
			user_type user_types_t,
			email character varying,
			username character varying
	);
	
	update	users b
	set		names = a.names,
			lastnames = a.lastnames,
			gender = a.gender,
			user_type = a.user_type,
			email = a.email,
			username = a.username
	from	tmp_users a
	where	b.id = a.id;
	
	select	id
	into	l_user_id
	from	tmp_users
	limit	1;
	
	if exists(select * from phones a where a.user_id = l_user_id) then
		update	phones b
		set		phone_number = a.phone
		from	tmp_users a
		where	b.user_id = a.id;
	else
		
		insert into phones(user_id, phone_type, phone_number)
		select	l_user_id,
				'principal',
				a.phone
		from	tmp_users a;
		
	end if;
	
	return 	true;
	
END; $_$;


ALTER FUNCTION public.fx_update_user(jsonb) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 223 (class 1259 OID 24902)
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categories (
    id integer NOT NULL,
    name character varying NOT NULL,
    description character varying
);


ALTER TABLE public.categories OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 24900)
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categories_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categories_id_seq OWNER TO postgres;

--
-- TOC entry 2975 (class 0 OID 0)
-- Dependencies: 222
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;


--
-- TOC entry 232 (class 1259 OID 33919)
-- Name: distributions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.distributions (
    sale_id integer NOT NULL,
    table_id integer NOT NULL,
    fraction_id integer NOT NULL,
    create_by integer NOT NULL,
    create_at abstime DEFAULT now()
);


ALTER TABLE public.distributions OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 24891)
-- Name: document_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.document_types (
    id integer NOT NULL,
    user_id integer NOT NULL,
    document_type character varying NOT NULL,
    document_number character varying NOT NULL
);


ALTER TABLE public.document_types OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 24889)
-- Name: document_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.document_types_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.document_types_id_seq OWNER TO postgres;

--
-- TOC entry 2976 (class 0 OID 0)
-- Dependencies: 220
-- Name: document_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.document_types_id_seq OWNED BY public.document_types.id;


--
-- TOC entry 215 (class 1259 OID 24611)
-- Name: documenttypes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.documenttypes (
    id integer NOT NULL,
    userid integer NOT NULL,
    documenttype character varying NOT NULL,
    documentnumber character varying NOT NULL
);


ALTER TABLE public.documenttypes OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 24609)
-- Name: documenttypes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.documenttypes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documenttypes_id_seq OWNER TO postgres;

--
-- TOC entry 2977 (class 0 OID 0)
-- Dependencies: 214
-- Name: documenttypes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.documenttypes_id_seq OWNED BY public.documenttypes.id;


--
-- TOC entry 234 (class 1259 OID 34009)
-- Name: fractions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fractions (
    id integer NOT NULL,
    init time without time zone NOT NULL,
    finish time without time zone NOT NULL
);


ALTER TABLE public.fractions OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 34007)
-- Name: fractions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fractions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fractions_id_seq OWNER TO postgres;

--
-- TOC entry 2978 (class 0 OID 0)
-- Dependencies: 233
-- Name: fractions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.fractions_id_seq OWNED BY public.fractions.id;


--
-- TOC entry 219 (class 1259 OID 24880)
-- Name: phones; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.phones (
    id integer NOT NULL,
    user_id integer NOT NULL,
    phone_type character varying,
    phone_number character varying
);


ALTER TABLE public.phones OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 24878)
-- Name: phones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.phones_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phones_id_seq OWNER TO postgres;

--
-- TOC entry 2979 (class 0 OID 0)
-- Dependencies: 218
-- Name: phones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.phones_id_seq OWNED BY public.phones.id;


--
-- TOC entry 225 (class 1259 OID 24913)
-- Name: products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.products (
    id integer NOT NULL,
    name character varying NOT NULL,
    description character varying,
    category_id integer NOT NULL,
    unit_price numeric(13,2) NOT NULL,
    photo character varying,
    status boolean DEFAULT true,
    create_at abstime DEFAULT now()
);


ALTER TABLE public.products OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 24911)
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.products_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_id_seq OWNER TO postgres;

--
-- TOC entry 2980 (class 0 OID 0)
-- Dependencies: 224
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.products_id_seq OWNED BY public.products.id;


--
-- TOC entry 229 (class 1259 OID 24958)
-- Name: sale_details; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sale_details (
    id integer NOT NULL,
    sale_id integer NOT NULL,
    product_id integer NOT NULL,
    unit_price numeric(13,2) NOT NULL,
    quantity integer NOT NULL,
    price numeric(13,2) NOT NULL
);


ALTER TABLE public.sale_details OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 24956)
-- Name: sale_details_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sale_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sale_details_id_seq OWNER TO postgres;

--
-- TOC entry 2981 (class 0 OID 0)
-- Dependencies: 228
-- Name: sale_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sale_details_id_seq OWNED BY public.sale_details.id;


--
-- TOC entry 231 (class 1259 OID 33770)
-- Name: sales; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sales (
    id integer NOT NULL,
    total_amount numeric(13,2) NOT NULL,
    disacount numeric(13,2) DEFAULT 0,
    customer_id integer NOT NULL,
    table_id integer NOT NULL,
    user_id integer NOT NULL,
    status public.status_t DEFAULT 'reserved'::public.status_t,
    date_reservation date,
    time_reservation time without time zone,
    persons_quantity integer,
    create_at abstime DEFAULT now(),
    user_modify integer,
    modify_at abstime DEFAULT now()
);


ALTER TABLE public.sales OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 33768)
-- Name: sales_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sales_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sales_id_seq OWNER TO postgres;

--
-- TOC entry 2982 (class 0 OID 0)
-- Dependencies: 230
-- Name: sales_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sales_id_seq OWNED BY public.sales.id;


--
-- TOC entry 227 (class 1259 OID 24926)
-- Name: tables; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tables (
    id integer NOT NULL,
    name character varying,
    number integer,
    photo character varying,
    create_at abstime DEFAULT now()
);


ALTER TABLE public.tables OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 24924)
-- Name: tables_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tables_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tables_id_seq OWNER TO postgres;

--
-- TOC entry 2983 (class 0 OID 0)
-- Dependencies: 226
-- Name: tables_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tables_id_seq OWNED BY public.tables.id;


--
-- TOC entry 217 (class 1259 OID 24867)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    names character varying NOT NULL,
    lastnames character varying NOT NULL,
    gender boolean,
    user_type public.user_types_t DEFAULT 'customer'::public.user_types_t,
    email character varying,
    username character varying,
    password character varying,
    create_at abstime DEFAULT now()
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 24865)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- TOC entry 2984 (class 0 OID 0)
-- Dependencies: 216
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 2815 (class 2604 OID 24905)
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);


--
-- TOC entry 2814 (class 2604 OID 24894)
-- Name: document_types id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_types ALTER COLUMN id SET DEFAULT nextval('public.document_types_id_seq'::regclass);


--
-- TOC entry 2809 (class 2604 OID 24614)
-- Name: documenttypes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documenttypes ALTER COLUMN id SET DEFAULT nextval('public.documenttypes_id_seq'::regclass);


--
-- TOC entry 2828 (class 2604 OID 34012)
-- Name: fractions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fractions ALTER COLUMN id SET DEFAULT nextval('public.fractions_id_seq'::regclass);


--
-- TOC entry 2813 (class 2604 OID 24883)
-- Name: phones id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.phones ALTER COLUMN id SET DEFAULT nextval('public.phones_id_seq'::regclass);


--
-- TOC entry 2816 (class 2604 OID 24916)
-- Name: products id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products ALTER COLUMN id SET DEFAULT nextval('public.products_id_seq'::regclass);


--
-- TOC entry 2821 (class 2604 OID 24961)
-- Name: sale_details id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sale_details ALTER COLUMN id SET DEFAULT nextval('public.sale_details_id_seq'::regclass);


--
-- TOC entry 2822 (class 2604 OID 33773)
-- Name: sales id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sales ALTER COLUMN id SET DEFAULT nextval('public.sales_id_seq'::regclass);


--
-- TOC entry 2819 (class 2604 OID 24929)
-- Name: tables id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tables ALTER COLUMN id SET DEFAULT nextval('public.tables_id_seq'::regclass);


--
-- TOC entry 2810 (class 2604 OID 24870)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 2838 (class 2606 OID 24910)
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- TOC entry 2836 (class 2606 OID 24899)
-- Name: document_types document_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_types
    ADD CONSTRAINT document_types_pkey PRIMARY KEY (id);


--
-- TOC entry 2830 (class 2606 OID 24619)
-- Name: documenttypes documenttypes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documenttypes
    ADD CONSTRAINT documenttypes_pkey PRIMARY KEY (id);


--
-- TOC entry 2848 (class 2606 OID 34014)
-- Name: fractions fractions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fractions
    ADD CONSTRAINT fractions_pkey PRIMARY KEY (id);


--
-- TOC entry 2834 (class 2606 OID 24888)
-- Name: phones phones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.phones
    ADD CONSTRAINT phones_pkey PRIMARY KEY (id);


--
-- TOC entry 2840 (class 2606 OID 24923)
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- TOC entry 2844 (class 2606 OID 24963)
-- Name: sale_details sale_details_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sale_details
    ADD CONSTRAINT sale_details_pkey PRIMARY KEY (id);


--
-- TOC entry 2846 (class 2606 OID 33779)
-- Name: sales sales_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sales
    ADD CONSTRAINT sales_pkey PRIMARY KEY (id);


--
-- TOC entry 2842 (class 2606 OID 24935)
-- Name: tables tables_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tables
    ADD CONSTRAINT tables_pkey PRIMARY KEY (id);


--
-- TOC entry 2832 (class 2606 OID 24877)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


-- Completed on 2020-02-13 08:48:23

--
-- PostgreSQL database dump complete
--


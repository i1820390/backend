-- Función de Inicio de Sesión
DROP FUNCTION IF EXISTS public.fx_login(
	character varying, 
	character varying,
	boolean);
	
CREATE OR REPLACE FUNCTION public.fx_login(character,character, boolean) 
RETURNS TABLE (
	id integer,
	names character varying,
	lastnames character varying,
	gender boolean,
	user_type user_types_t,
	email character varying,
	username character varying,
	password character varying,
	create_at abstime
) 
AS $$
DECLARE
	p_username	alias for $1;
	p_password	alias for $2;
	p_with_email	alias for $3;
BEGIN
	if p_with_email then
		return 	query
		select 	*
		from	users a
		where	a.email = p_username;
	else 
	
		return 	query
		select 	* 
		from 	users a
		where	a.username = p_username
		and 	a.password = p_password;
	end if;
	
END; $$ 
LANGUAGE 'plpgsql';

-- FUNCTION: public.fx_create_users(jsonb)

DROP FUNCTION IF EXISTS public.fx_create_users(jsonb);

CREATE OR REPLACE FUNCTION public.fx_create_users(
	jsonb)
    RETURNS boolean
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
DECLARE
	p_json	alias for $1;	
	
	email_unique 		character varying;
	username_unique 	character varying;
BEGIN
	
	drop table if exists tmp_users;
	create temporary table tmp_users as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			names character varying,
			lastnames character varying,
			gender boolean,
			user_type user_types_t,
			email character varying,
			username character varying,
			password character varying
	);
	
	select	username,
			email
	into	username_unique,
			email_unique
	from	tmp_users
	limit 	1;
	
	if exists (select * from users where username = username_unique or email = email_unique) then
		return false;
	else
		insert 	into users(names, lastnames, gender, user_type, email, username, password)
		select	*
		from	tmp_users a;
		
		return true;
	end if;

END; $BODY$;

ALTER FUNCTION public.fx_create_users(jsonb)
    OWNER TO postgres;



-- Función de registro de productos
DROP FUNCTION IF EXISTS public.fx_create_products(jsonb);
	
CREATE OR REPLACE FUNCTION public.fx_create_products(jsonb) 
RETURNS BOOLEAN 
AS $$
DECLARE
	p_json	alias for $1;	
BEGIN
	
	drop table if exists tmp_products;
	create temporary table tmp_products as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			name character varying,
			description character varying,
			category_id integer,
			unit_price numeric(13,2),
			photo character varying
	);
	
	insert 	into products(name, description, category_id, unit_price, photo)
	select	*
	from	tmp_products a;
	
	return 	true;
	
END; $$ 
LANGUAGE 'plpgsql';



-- Función de registro de categorias
DROP FUNCTION IF EXISTS public.fx_create_categories(jsonb);
	
CREATE OR REPLACE FUNCTION public.fx_create_categories(jsonb) 
RETURNS BOOLEAN 
AS $$
DECLARE
	p_json	alias for $1;	
BEGIN
	
	drop table if exists tmp_categories;
	create temporary table tmp_categories as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			name character varying,
			description character varying
	);
	
	insert 	into categories(name, description)
	select	*
	from	tmp_categories a;
	
	return 	true;
	
END; $$ 
LANGUAGE 'plpgsql';

-- public.fx_get_categories

-- Función de registro de categorias
DROP FUNCTION IF EXISTS public.fx_get_categories();
	
CREATE OR REPLACE FUNCTION public.fx_get_categories() 
RETURNS TABLE(
	id integer,
	name character varying,
	description character varying
)
AS $$

BEGIN
	return 	query
	select	* 
	from	categories;
	
END; $$ 
LANGUAGE 'plpgsql';


-- Función de registro de categorias
DROP FUNCTION IF EXISTS public.fx_get_products();
	
CREATE OR REPLACE FUNCTION public.fx_get_products() 
RETURNS TABLE(
	id integer,
	name character varying,
	description character varying,
	category_id integer,
	unit_price numeric(13,2),
	photo character varying,
	status boolean,
	create_at abstime
)
AS $$

BEGIN
	return 	query
	select	* 
	from	products;
	
END; $$ 
LANGUAGE 'plpgsql';


-- Función de registro de categorias
DROP FUNCTION IF EXISTS public.fx_get_document_types(integer);
	
CREATE OR REPLACE FUNCTION public.fx_get_document_types(integer) 
RETURNS TABLE(
	id integer,
	user_id integer,
	document_type character varying,
	document_number character varying
)
AS $$
DECLARE
	p_user_id	alias for $1;
BEGIN
	return 	query
	select	* 
	from	document_types
	where	user_id = p_user_id;
	
END; $$ 
LANGUAGE 'plpgsql';


-- Función de registro de categorias
DROP FUNCTION IF EXISTS public.fx_get_phones(integer);
	
CREATE OR REPLACE FUNCTION public.fx_get_phones(integer) 
RETURNS TABLE(
	id integer,
	user_id integer,
	phone_type character varying,
	phone_number character varying
)
AS $$
DECLARE
	p_user_id	alias for $1;
BEGIN
	return 	query
	select	* 
	from	phones p
	where	p.user_id = p_user_id;
	
END; $$ 
LANGUAGE 'plpgsql';


-- Función de registro de categorias
DROP FUNCTION IF EXISTS public.fx_create_sales(jsonb);
	
CREATE OR REPLACE FUNCTION public.fx_create_sales(jsonb) 
RETURNS TABLE (id integer, email character varying)
AS $$
DECLARE
	p_json	alias for $1;
BEGIN
	
	drop table if exists tmp_sales;
	create temporary table tmp_sales as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			total_amount numeric(13,2),
			customer_id integer,
			table_id integer,
			user_id integer,
			date_reservation date,
			time_reservation time,
			persons_quantity integer
	);
	
	insert 	into sales(total_amount, customer_id, table_id, user_id, date_reservation, time_reservation, persons_quantity)
	select	*
	from	tmp_sales a;
	
	return 	query
	select	a.id,
			b.email
	from	sales a
			inner join users b
			on a.customer_id = b.id 
	order 	by modify_at desc
	limit	1
	;
	
END; $$ 
LANGUAGE 'plpgsql';

-- Función de registro de categorias
DROP FUNCTION IF EXISTS public.fx_get_sales(integer);
	
CREATE OR REPLACE FUNCTION public.fx_get_sales(integer) 
RETURNS TABLE (
	id integer,
	total_amount numeric(13,2),
	disacount numeric(13,2),
	customer_id integer,
	table_id integer,
	user_id integer,
	status status_t,
	date_reservation date,
	time_reservation time,
	persons_quantity integer,
	create_at timestamp,
	user_modify integer,
	modify_at timestamp,
	names character varying,
	lastnames character varying,
	username character varying
)
AS $$
DECLARE
	p_customer	alias for $1;
BEGIN
	
	if p_customer != 0 then
		return 	query
		select 	a.*,
				b.names,
				b.lastnames,
				b.username
		from	sales a
				inner join users b
					on a.customer_id = b.id
		where	a.customer_id = p_customer
		and		a.status = 'reserved';
	else
		return 	query
		select 	a.*,
				b.names,
				b.lastnames,
				b.username
		from	sales a
				inner join users b
					on a.customer_id = b.id
		where	a.status = 'reserved';
	end if;

END; $$ 
LANGUAGE 'plpgsql';

-- select * from public.fx_get_sales(1);

-- Función de registro de categorias
DROP FUNCTION IF EXISTS public.fx_create_sale_details(jsonb);
	
CREATE OR REPLACE FUNCTION public.fx_create_sale_details(jsonb) 
RETURNS BOOLEAN
AS $$
DECLARE
	p_json	alias for $1;
BEGIN
	
	drop table if exists tmp_sale_details;
	create temporary table tmp_sale_details as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			sale_id integer,
			product_id integer,
			unit_price numeric(13,2),
			quantity integer,
			price numeric(13,2)
	);
	
	insert 	into sale_details(sale_id, product_id, unit_price, quantity, price)
	select	*
	from	tmp_sale_details a;
	
	return 	true;
	
END; $$ 
LANGUAGE 'plpgsql';


-- Función de registro de categorias
DROP FUNCTION IF EXISTS public.fx_get_tables();
	
CREATE OR REPLACE FUNCTION public.fx_get_tables() 
RETURNS TABLE(
	id integer,
	name character varying,
	number integer,
	photo character varying,
	create_at abstime
)
AS $$
DECLARE
	
BEGIN
	
	return 	query
	select	*
	from 	tables;
	
END; $$ 
LANGUAGE 'plpgsql';



-- Función de registro de categorias
DROP FUNCTION IF EXISTS public.fx_get_sale_detail_by_sale(integer);
	
CREATE OR REPLACE FUNCTION public.fx_get_sale_detail_by_sale(integer) 
RETURNS TABLE(
	id integer,
	sale_id integer,
	product_id integer,
	unit_price numeric(13,2),
	quantity integer,
	price numeric(13,2)
)
AS $$
DECLARE
	p_sale_id	alias for $1;
BEGIN
	
	return 	query
	select 	*
	from	sale_details a
	where	a.sale_id = p_sale_id;
	
END; $$ 
LANGUAGE 'plpgsql';



-- Función de registro de categorias
DROP FUNCTION IF EXISTS public.fx_update_sale(jsonb);
	
CREATE OR REPLACE FUNCTION public.fx_update_sale(jsonb) 
RETURNS BOOLEAN
AS $$
DECLARE
	p_json	alias for $1;
BEGIN

	drop table if exists tmp_sales;
	create temporary table tmp_sales as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			id integer,
			total_amount numeric(13,2),
			customer_id integer,
			table_id integer,
			user_id integer,
			date_reservation date,
			time_reservation time,
			persons_quantity integer
	);
	
	update	sales b
	set	table_id = a.table_id,
		date_reservation = a.date_reservation,
		time_reservation = a.time_reservation,
		persons_quantity = a.persons_quantity,
		total_amount = a.total_amount
	from	tmp_sales a
	where	b.id = a.id;
	
	return 	true;
	
	
END; $$ 
LANGUAGE 'plpgsql';



-- Función de registro de categorias
DROP FUNCTION IF EXISTS public.fx_update_sale_details(integer,jsonb);
	
CREATE OR REPLACE FUNCTION public.fx_update_sale_details(integer, jsonb) 
RETURNS BOOLEAN
AS $$
DECLARE
	p_sale_id	alias for $1;
	p_json		alias for $2;
BEGIN
	
	drop table if exists tmp_sale_details;
	create temporary table tmp_sale_details as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			sale_id integer,
			product_id integer,
			unit_price numeric(13,2),
			quantity integer,
			price numeric(13,2)
	);

	-- eliminar e insertar
	delete 	from sale_details
	where	sale_id = p_sale_id;
	
	insert 	into sale_details(sale_id, product_id, unit_price, quantity, price)
	select	*
	from	tmp_sale_details a;
	
	return 	true;
	
END; $$ 
LANGUAGE 'plpgsql';



-- Función de registro de categorias
DROP FUNCTION IF EXISTS public.fx_update_tables(jsonb);
	
CREATE OR REPLACE FUNCTION public.fx_update_tables(jsonb) 
RETURNS BOOLEAN
AS $$
DECLARE
	p_json		alias for $1;
BEGIN

	drop table if exists tmp_tables;
	create temporary table tmp_tables as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			id integer,
			name character varying,
			number integer,
			photo character varying
	);

	-- eliminar e insertar
	update	tables b
	set	name = a.name,
		number = a.number,
		photo = a.photo,
		create_at = now()
	from	tmp_tables a
	where	b.id = a.id;
	
	return 	true;
	
END; $$ 
LANGUAGE 'plpgsql';


-- Función de registro de categorias
DROP FUNCTION IF EXISTS public.fx_update_tables(jsonb);
	
CREATE OR REPLACE FUNCTION public.fx_update_tables(jsonb) 
RETURNS BOOLEAN
AS $$
DECLARE
	p_json		alias for $1;
BEGIN

	drop table if exists tmp_tables;
	create temporary table tmp_tables as
	select	*
	from	jsonb_to_recordset(p_json)
	as		f(
			id integer,
			name character varying,
			number integer,
			photo character varying
	);

	-- eliminar e insertar
	update	tables b
	set	name = a.name,
		number = a.number,
		photo = a.photo,
		create_at = now()
	from	tmp_tables a
	where	b.id = a.id;
	
	return 	true;
	
END; $$ 
LANGUAGE 'plpgsql';


/*

select 	*
	from	sale_details a
	where	a.sale_id = 18;
	

*/

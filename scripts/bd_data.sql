--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

-- Started on 2020-02-13 08:47:59

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2979 (class 0 OID 24902)
-- Dependencies: 223
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categories (id, name, description) FROM stdin;
1	plato de comida	categoria de platos de comida
2	bebidas	categoria de bebidas
\.


--
-- TOC entry 2988 (class 0 OID 33919)
-- Dependencies: 232
-- Data for Name: distributions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.distributions (sale_id, table_id, fraction_id, create_by, create_at) FROM stdin;
9	7	2	0	2020-02-11 00:28:39-05
9	7	3	0	2020-02-11 00:28:39-05
9	7	4	0	2020-02-11 00:28:39-05
9	7	5	0	2020-02-11 00:28:39-05
9	7	6	0	2020-02-11 00:28:39-05
9	7	7	0	2020-02-11 00:28:39-05
9	7	8	0	2020-02-11 00:28:39-05
10	8	2	0	2020-02-12 07:00:40-05
10	8	3	0	2020-02-12 07:00:40-05
10	8	4	0	2020-02-12 07:00:40-05
10	8	5	0	2020-02-12 07:00:40-05
11	8	2	0	2020-02-12 07:11:37-05
11	8	3	0	2020-02-12 07:11:37-05
11	8	4	0	2020-02-12 07:11:37-05
11	8	5	0	2020-02-12 07:11:37-05
11	8	6	0	2020-02-12 07:11:37-05
13	7	2	0	2020-02-12 07:19:42-05
13	7	3	0	2020-02-12 07:19:42-05
13	7	4	0	2020-02-12 07:19:42-05
13	7	5	0	2020-02-12 07:19:42-05
13	7	6	0	2020-02-12 07:19:42-05
13	7	7	0	2020-02-12 07:19:42-05
13	7	8	0	2020-02-12 07:19:42-05
13	7	9	0	2020-02-12 07:19:42-05
13	7	10	0	2020-02-12 07:19:42-05
13	7	11	0	2020-02-12 07:19:42-05
13	7	12	0	2020-02-12 07:19:42-05
\.


--
-- TOC entry 2977 (class 0 OID 24891)
-- Dependencies: 221
-- Data for Name: document_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.document_types (id, user_id, document_type, document_number) FROM stdin;
\.


--
-- TOC entry 2971 (class 0 OID 24611)
-- Dependencies: 215
-- Data for Name: documenttypes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.documenttypes (id, userid, documenttype, documentnumber) FROM stdin;
\.


--
-- TOC entry 2990 (class 0 OID 34009)
-- Dependencies: 234
-- Data for Name: fractions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fractions (id, init, finish) FROM stdin;
1	12:00:00	12:30:00
2	12:30:00	13:00:00
3	13:00:00	13:30:00
4	13:30:00	14:00:00
5	14:00:00	14:30:00
6	14:30:00	15:00:00
7	15:00:00	15:30:00
8	15:30:00	16:00:00
9	16:00:00	16:30:00
10	16:30:00	17:00:00
11	17:00:00	17:30:00
12	17:30:00	18:00:00
13	18:00:00	18:30:00
14	18:30:00	19:00:00
15	19:00:00	19:30:00
16	19:30:00	20:00:00
17	20:00:00	20:30:00
18	20:30:00	21:00:00
19	21:00:00	21:30:00
\.


--
-- TOC entry 2975 (class 0 OID 24880)
-- Dependencies: 219
-- Data for Name: phones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.phones (id, user_id, phone_type, phone_number) FROM stdin;
1	10	principal	956856956
2	11	principal	956489658
3	7	principal	123456789
\.


--
-- TOC entry 2981 (class 0 OID 24913)
-- Dependencies: 225
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.products (id, name, description, category_id, unit_price, photo, status, create_at) FROM stdin;
15	inca kola personal	bebida para complementar	2	3.50	product-1580875845111.jpg	t	2020-02-04 23:10:45-05
16	Coca cola	bebida para complementar	2	3.50	product-1580875963361.jpg	t	2020-02-04 23:12:43-05
17	lomo saltado	papa con tomates y cebollas, lleva carne	1	12.50	product-1580876040060.jpg	t	2020-02-04 23:14:00-05
18	escabeche	mucha cebolla y papas con jugo.	1	12.50	product-1580876117286.jfif	t	2020-02-04 23:15:17-05
19	Ceviche a la piedra	una variedad de ceviche para consumo	1	22.80	product-1580876720207.jpg	t	2020-02-04 23:25:20-05
20	arroz con pollo	arroz y pollo juntos	1	25.90	product-1580876831448.jpg	t	2020-02-04 23:27:12-05
22	escabeche	mucha cebolla y papas con jugo.	1	12.50	product-1581135737006.jpg	t	2020-02-07 23:22:17-05
\.


--
-- TOC entry 2985 (class 0 OID 24958)
-- Dependencies: 229
-- Data for Name: sale_details; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sale_details (id, sale_id, product_id, unit_price, quantity, price) FROM stdin;
124	9	22	12.50	5	62.50
125	9	19	22.80	4	91.20
126	9	15	3.50	6	21.00
127	9	16	3.50	3	10.50
128	10	22	12.50	4	50.00
129	10	20	25.90	3	77.70
130	11	22	12.50	3	37.50
131	11	20	25.90	4	103.60
134	13	22	12.50	3	37.50
135	13	19	22.80	3	68.40
\.


--
-- TOC entry 2987 (class 0 OID 33770)
-- Dependencies: 231
-- Data for Name: sales; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sales (id, total_amount, disacount, customer_id, table_id, user_id, status, date_reservation, time_reservation, persons_quantity, create_at, user_modify, modify_at) FROM stdin;
9	185.20	0.00	11	7	0	reserved	2020-02-12	\N	6	2020-02-11 00:28:39-05	\N	2020-02-11 00:28:39-05
10	127.70	0.00	11	8	0	reserved	2020-02-28	\N	1	2020-02-12 07:00:39-05	\N	2020-02-12 07:00:39-05
11	141.10	0.00	11	8	0	reserved	2020-02-05	\N	1	2020-02-12 07:11:37-05	\N	2020-02-12 07:11:37-05
13	105.90	0.00	1	7	0	reserved	2020-02-28	\N	4	2020-02-12 07:19:42-05	\N	2020-02-12 07:19:42-05
\.


--
-- TOC entry 2983 (class 0 OID 24926)
-- Dependencies: 227
-- Data for Name: tables; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tables (id, name, number, photo, create_at) FROM stdin;
5	comun 2	1	comun_2.jpg	2020-01-30 00:39:10-05
6	lujo 4	2	lujo_4.jpg	2020-01-30 00:39:10-05
7	solito	3	solito.jpg	2020-01-30 00:39:10-05
8	vidrio_1	4	vidrio_1.jpg	2020-01-30 00:39:10-05
9	casual 1	5	casual_1.jpg	2020-01-30 00:39:10-05
12	marron	13	table-1581130591960.jpg	2020-02-06 00:46:47-05
\.


--
-- TOC entry 2973 (class 0 OID 24867)
-- Dependencies: 217
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, names, lastnames, gender, user_type, email, username, password, create_at) FROM stdin;
1	jean karlo	diaz espejo	t	\N	jeancde3@gmail.com	jdiaze	e10adc3949ba59abbe56e057f20f883e	2020-01-23 23:07:22-05
2	yesenia mariela	quispe aguilar	f	\N	jeancde3@gmail.com	jquispea	e10adc3949ba59abbe56e057f20f883e	2020-01-23 23:07:22-05
8	Jean	Diaz	t	\N	jeandiazc@gmail.com	jdiaze1	e10adc3949ba59abbe56e057f20f883e	2020-02-04 06:48:12-05
9	Yessenia	Quispe	f	\N	jeancde34@gmail.com	yessi123	e10adc3949ba59abbe56e057f20f883e	2020-02-04 06:50:20-05
10	Niza	torreto	f	\N	desmotivadorxs@gmail.com	nizacons	e10adc3949ba59abbe56e057f20f883e	2020-02-07 22:19:41-05
11	Joe	Doe	f	customer	47006172@continental.edu.pe	joedoe	e10adc3949ba59abbe56e057f20f883e	2020-02-07 22:35:39-05
7	mariela	cruz	f	customer	mcruz@gmail.com	mcruz	25f9e794323b453885f5181f1b624d0b	2020-02-04 06:36:14-05
\.


--
-- TOC entry 3006 (class 0 OID 0)
-- Dependencies: 222
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categories_id_seq', 2, true);


--
-- TOC entry 3007 (class 0 OID 0)
-- Dependencies: 220
-- Name: document_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.document_types_id_seq', 1, false);


--
-- TOC entry 3008 (class 0 OID 0)
-- Dependencies: 214
-- Name: documenttypes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.documenttypes_id_seq', 1, false);


--
-- TOC entry 3009 (class 0 OID 0)
-- Dependencies: 233
-- Name: fractions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fractions_id_seq', 19, true);


--
-- TOC entry 3010 (class 0 OID 0)
-- Dependencies: 218
-- Name: phones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.phones_id_seq', 3, true);


--
-- TOC entry 3011 (class 0 OID 0)
-- Dependencies: 224
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.products_id_seq', 22, true);


--
-- TOC entry 3012 (class 0 OID 0)
-- Dependencies: 228
-- Name: sale_details_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sale_details_id_seq', 135, true);


--
-- TOC entry 3013 (class 0 OID 0)
-- Dependencies: 230
-- Name: sales_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sales_id_seq', 13, true);


--
-- TOC entry 3014 (class 0 OID 0)
-- Dependencies: 226
-- Name: tables_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tables_id_seq', 12, true);


--
-- TOC entry 3015 (class 0 OID 0)
-- Dependencies: 216
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 11, true);


-- Completed on 2020-02-13 08:48:02

--
-- PostgreSQL database dump complete
--


﻿-- CREACION DE DB --
--create database chaufanBD;

-- CREACION DE OBJETOS --
drop type if exists user_types_t;
create type user_types_t as enum('customer', 'admin', 'user');

drop table if exists users;
create table users (
	id serial not null primary key,
	names character varying not null,
	lastnames character varying not null,
	gender boolean,
	user_type user_types_t default 'customer',
	email character varying,
	username character varying,
	password character varying,
	create_at abstime default now()
);

drop table if exists phones;
create table phones (
	id serial not null primary key,
	user_id integer not null,
	phone_type character varying,
	phone_number character varying
);

drop table if exists document_types;
create table document_types (
	id serial not null primary key,
	user_id integer not null,
	document_type character varying not null,
	document_number character varying not null
);

drop table if exists categories;
create table categories (
	id serial not null primary key,
	name character varying not null,
	description character varying
);

drop table if exists products;
create table products (
	id serial not null primary key,
	name character varying not null,
	description character varying,
	category_id integer not null,
	unit_price numeric(13,2) not null,
	photo character varying,
	status boolean default true,
	create_at abstime default now()
);

drop table if exists tables;
create table tables (
	id serial not null primary key,
	name character varying,
	number integer,
	photo character varying,
	create_at abstime default now()
);

drop type if exists status_t;
create type status_t as enum('reserved', 'inProcess', 'attended', 'cancelled');

drop table if exists sales;
create table sales (
	id serial not null primary key,
	total_amount numeric(13,2) not null,
	disacount numeric(13,2) default 0,
	customer_id integer not null,
	table_id integer not null,
	user_id integer not null,
	status status_t default 'reserved',
	date_reservation date,
	time_reservation time,
	persons_quantity integer,
	create_at abstime default now(),
	user_modify integer,
	modify_at abstime default now()
);

drop table if exists sale_details;
create table sale_details (
	id serial not null primary key,
	sale_id integer not null,
	product_id integer not null,
	unit_price numeric(13,2) not null,
	quantity integer not null,
	price numeric(13,2) not null
);

